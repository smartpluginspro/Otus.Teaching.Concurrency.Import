﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomController : ControllerBase
    {
        ICustomerRepository _context = CustomerRepository.GetRepository();

        // GET api/<CustomController>/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Customer>> Get(int id)
        {
            var customer = await _context.GetCustomer(id);

            if(customer == null)
                return NotFound();
            else
                return Ok(customer);
        }

        // DELETE api/<CustomController>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            var result = await _context.DeleteCustomerAsync(id);

            if(result == true)
                return Ok(result);
            else
                return NotFound();
        }

        // ADD api/<CustomController>/6
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<bool>> Add([FromBody] Customer customer)
        {
            var result = await _context.AddCustomerAsync(customer);

            if(result==true)
                return Ok(result);
            else
                return Conflict();
        }
    }
}
