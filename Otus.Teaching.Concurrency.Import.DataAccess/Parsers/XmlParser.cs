﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<IEnumerable<Customer>>
    {
        private readonly string _dataFilePath;

        public XmlParser(string dataFilePath)
        {
            _dataFilePath = dataFilePath;
        }

        public IEnumerable<Customer> Parse()
        {
            using (var reader = new FileStream(_dataFilePath, FileMode.Open))
            {
                var deserializer = new XmlSerializer(typeof(CustomersList));
                var customersList = (CustomersList)deserializer.Deserialize(reader);
                return customersList.Customers;
            }
        }
    }
}