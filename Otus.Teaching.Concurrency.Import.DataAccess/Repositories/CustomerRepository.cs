﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LiteDB;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly string _dbName = @"C:\ProgramData\TempFiles\customersDB.db";
        private readonly LiteDatabase _liteDatabase;
        private static CustomerRepository _repository;
        private ILiteCollection<Customer> _dbCllection;
        private object _locker = new object();

        private CustomerRepository()
        {
            _liteDatabase = new LiteDatabase(_dbName);
            _dbCllection = _liteDatabase.GetCollection<Customer>("customers");
        }

        public async Task<bool> AddCustomerAsync(Customer customer)
        {
            return await Task.Run(() =>
            {
                try
                {
                    lock (_locker)
                        _dbCllection.Insert(customer);

                    return true;
                }
                catch (Exception ex)
                {
                    //TODO: Добавить логгирование
                    return false;
                }
            });
         }

        public static CustomerRepository GetRepository()
        {
            if (_repository == null)
                _repository = new CustomerRepository();

            return _repository;
        }

        public async Task<Customer> GetCustomer(int id)
        {
            return await Task.Run(() =>
            {
                lock (_locker)
                    return _dbCllection.Find(c => c.Id == id).FirstOrDefault();
            });
        }

        public void Dispose()
        {
            _liteDatabase.Dispose();
        }

        public async Task<bool> DeleteCustomerAsync(int id)
        {
            return await Task.Run(() =>
            {
                lock (_locker)
                    return _dbCllection.Delete(id);
            });
        }
    }
}