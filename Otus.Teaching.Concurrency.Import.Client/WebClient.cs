﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Client
{
    internal class WebClient
    {
        private readonly HttpClient _httpClient;
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        public WebClient()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:5001") 
            };

            _jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        }

        public async Task<Customer?> GetCustomerById(int id)
        {
            var responce = await _httpClient.GetStringAsync($"/api/Custom/{id}");
            return JsonSerializer.Deserialize<Customer>(responce, _jsonSerializerOptions);
        }

        public async Task<bool> AddCustomer(Customer customer)
        {
            var json = JsonSerializer.Serialize(customer);
            var data = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await _httpClient.PostAsync("/api/Custom/", data);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteCustomer(int id)
        {
            var response = await _httpClient.DeleteAsync($"/api/Custom/{id}");
            return response.IsSuccessStatusCode;
        }
    }
}
