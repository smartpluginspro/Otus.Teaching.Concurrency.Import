﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.Client
{
    internal class CustomersManager
    {
        private readonly ConsoleLogger _consoleLogger = new ConsoleLogger();
        private readonly WebClient _customerManager = new WebClient();

        public void Start()
        {
            _consoleLogger.WriteMessage("1 - get user by id\n2 - add user\n3 - delete user by id", ConsoleColor.Blue);
            var userInput = Console.ReadLine();

            switch (userInput)
            {
                case "1": 
                    GetCustomer();
                    break;
                case "2":
                    AddCustomer();
                    break;
                case "3":
                    DeleteUser();
                    break;
                default:
                    _consoleLogger.WriteMessage("Command is invalid!", ConsoleColor.Red);
                    break;
            }
            _consoleLogger.WriteMessage("\nRestart? y/n", ConsoleColor.Blue);
            var restartInput = Console.ReadLine();

            if (restartInput == "y")
                Start();
        }

        private void GetCustomer()
        {
            _consoleLogger.WriteMessage("Enter Customer Id: ");

            if (int.TryParse(Console.ReadLine(), out int id))
            {
                try
                {
                    var customer = _customerManager.GetCustomerById(id);

                    _consoleLogger.WriteMessage(customer?.Result?.ToString(), ConsoleColor.Green);
                }
                catch (Exception ex)
                {
                    _consoleLogger.WriteMessage(ex.Message, ConsoleColor.Red);
                }
            }
            else
            {
                _consoleLogger.WriteMessage("Id is invalid!", ConsoleColor.Red);
            }
        }

        private void AddCustomer()
        {
            try
            {
                var customer = RandomCustomerGenerator.Generate(1).First();
                customer.Id = new Random().Next(1, 2000);

                var result = _customerManager.AddCustomer(customer);

                if (result.Result)
                {
                    _consoleLogger.WriteMessage($"Customer is added!", ConsoleColor.Green);
                    _consoleLogger.WriteMessage(customer.ToString(), ConsoleColor.Gray);
                }
                else
                {
                    _consoleLogger.WriteMessage($"Customer is not added!", ConsoleColor.Red);
                    _consoleLogger.WriteMessage(customer.ToString(), ConsoleColor.Gray);
                }
            }
            catch (Exception ex)
            {
                _consoleLogger.WriteMessage(ex.Message, ConsoleColor.Red);
            }
        }

        private void DeleteUser()
        {
            _consoleLogger.WriteMessage("Enter Customer Id for delete: ");
            if (int.TryParse(Console.ReadLine(), out int id))
            {
                try
                {
                    var customer = _customerManager.GetCustomerById(id);
                    var result = _customerManager.DeleteCustomer(id);

                    _consoleLogger.WriteMessage($"Customer is delete!", ConsoleColor.Green);
                    _consoleLogger.WriteMessage(customer?.Result?.ToString(), ConsoleColor.Gray);
                }
                catch (Exception ex)
                {
                    _consoleLogger.WriteMessage(ex.Message, ConsoleColor.Red);
                }
            }
            else
            {
                _consoleLogger.WriteMessage("Id is invalid!", ConsoleColor.Red);
            }
        }
    }
}
