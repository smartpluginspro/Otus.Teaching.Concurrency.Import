﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Client
{
    internal class ConsoleLogger
    {
        public void ClearConsol() => Console.Clear();

        public void WriteMessage(string? message, ConsoleColor consoleColor = ConsoleColor.White)
        {
            if (message == null)
                return;
            
            var currentColor = Console.ForegroundColor;
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(message);
            Console.ForegroundColor = currentColor;
        }
    }
}
