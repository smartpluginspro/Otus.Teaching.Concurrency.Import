﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    internal class ThreadsDataLoader : IDataLoader
    {
        private readonly IEnumerable<Customer> _customers;
        private readonly int _processorCount;
        private readonly CustomerRepository _customerRepository = CustomerRepository.GetRepository();

        public ThreadsDataLoader(IEnumerable<Customer> customers, int processorCount)
        {
            _customers = customers;
            _processorCount = processorCount;
        }
        public void LoadData()
        {
            var take = _customers.Count() / _processorCount;

            var threads = new List<Thread>();

            for (var i = 0; i <= _processorCount; i++)
            {
                var currendData = _customers.Skip(i * take).Take(take);

                var thread = new Thread(() =>
                {
                    foreach (var item in currendData)
                        _customerRepository.AddCustomerAsync(item);
                });

                threads.Add(thread);
                thread.Start();
            }

            foreach (var thread in threads)
                thread.Join();
        }
    }
}
