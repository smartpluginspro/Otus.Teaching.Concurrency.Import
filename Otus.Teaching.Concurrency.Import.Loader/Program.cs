﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");
        
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();

            var customers = new XmlParser(_dataFilePath).Parse();

            var loader = new ThreadsDataLoader(customers, 8);

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            loader.LoadData();

            stopwatch.Stop();

            Console.WriteLine($"Load data: {stopwatch.Elapsed.TotalSeconds} sec");
            Console.ReadKey();
        }

        static void GenerateCustomersDataFile()
        {
            Console.WriteLine("Enter the quantity:");
            var userInputQuantity = Console.ReadLine();

            int.TryParse(userInputQuantity, out var quantity);

            if (quantity == 0)
                throw new Exception("Incorrect quality!");

            Console.WriteLine("0 - start as process, other - method start");
            var userInput = Console.ReadLine();

            if (userInput == "0")
            {
                var currentDirectory = Directory.GetCurrentDirectory()
                    .Replace("Otus.Teaching.Concurrency.Import.Loader", "Otus.Teaching.Concurrency.Import.DataGenerator.App");

                var path = Path.Combine(currentDirectory, "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe");
                var process = Process.Start(path, new string[]{ "customers", quantity.ToString()});
                Console.WriteLine($"DataGenerator started with process Id {process.Id}...");
                process.WaitForExit();
            }
            else
            {
                Console.WriteLine($"DataGenerator started as method");
                var xmlGenerator = new XmlGenerator(_dataFilePath, quantity);
                xmlGenerator.Generate();
            }
        }
    }
}