using System;
using System.Text.Json;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    [Serializable]
    [XmlRoot(nameof(Customer))]
    public class Customer
    {
        [XmlElement(nameof(Id))]
        public int Id { get; set; }

        [XmlElement(nameof(FullName))]
        public string FullName { get; set; }

        [XmlElement(nameof(Email))]
        public string Email { get; set; }

        [XmlElement(nameof(Phone))]
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"Customer Id: {Id},\nFullName: {FullName},\nEmail: {Email},\nPhone: {Phone}";
        }
    }
}