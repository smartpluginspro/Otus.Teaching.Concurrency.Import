using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        Task<bool> AddCustomerAsync(Customer customer);

        Task<bool> DeleteCustomerAsync(int id);

        Task<Customer> GetCustomer(int id);
    }
}